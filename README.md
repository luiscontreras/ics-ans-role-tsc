# ics-ans-role-tsc

Ansible role to install tsc kernel modules.

## Role Variables

```yaml
tsc_rpm_version: 4.0.5-0
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-tsc
```

## License

BSD 2-clause
